<?php

/**
 * @file
 * Allows Drupal Commerce to accept payments with the Skipjack Payment Gateway
 */

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_skipjack_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_skipjack'] = array(
    'title' => t('Skipjack Payment Gateway'),
    'display_title' => t('Credit Card'),
    'short_title' => t('Skipjack CC'),
    'description' => t('Allows customers to pay with a credit card via the Skipjack API.'),
    'active' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_skipjack_settings_form($settings = NULL) {
  if (!function_exists('curl_init')) {
    drupal_set_message(t('The Skipjack service requires cURL. Please talk to your system administrator to get this configured.'));
  }

  $form = array();

  $settings = (array) $settings + array(
    'skipjack_authorize_server' => 'development',
    'skipjack_login_serial_number' => '',
    'skipjack_developer_serial_number' => '',
  );

  $form["skipjack_authorize_server"] = array(
    '#type' => 'select',
    '#title' => t('Skipjack transaction server'),
    '#default_value' => $settings['skipjack_authorize_server'],
    '#options' => array(
      'development' => t('Development'),
      'production' => t('Production'),
    ),
  );

  $form["skipjack_login_serial_number"] = array(
    '#type' => 'textfield',
    '#title' => t('HTML Serial Number'),
    '#default_value' => $settings['skipjack_login_serial_number'],
    '#size' => 12,
    '#maxlength' => 20,
    '#required' => TRUE,
  );

  $form["skipjack_developer_serial_number"] = array(
    '#type' => 'textfield',
    '#title' => t('Developer Serial Number'),
    '#default_value' => $settings['skipjack_developer_serial_number'],
    '#size' => 12,
    '#maxlength' => 20,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_skipjack_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  return commerce_payment_credit_card_form(array('code' => ''));
}

/**
 * Payment method callback: checkout form validation.
 */
function commerce_skipjack_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  // Validate the credit card fields.
  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );

  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    return FALSE;
  }
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_skipjack_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();

  $cc_info = $pane_values['credit_card'];

  $order_string = '';

  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'product') {

      $product_quantity = $line_item_wrapper->quantity->value();
      $product_sku = $line_item_wrapper->commerce_product->sku->value();
      $product_title = substr($line_item_wrapper->commerce_product->title->value(), 0, 60);
      $unit_price = $line_item_wrapper->commerce_unit_price->value();
      $product_price = commerce_currency_amount_to_decimal($unit_price['amount'], $unit_price['currency_code']);

      $order_string .= $product_sku . "~" . $product_title . "~" . $product_price . "~" . $product_quantity . "~N~||";
    }
  }

  $decimal_amount = commerce_currency_amount_to_decimal($charge['amount'], $charge['currency_code']);
  $currency = commerce_currency_load($charge['currency_code']);

  /* We have to format the total price, because if the price is a round number,
   * no decimal points are added with just a regular call to
   * commerce_currency_amount_to_decimal(), and the transaction amount must be
   * at least 3 digits when sent to Skipjack.
   */
  $transaction_amount = number_format(commerce_currency_round(abs($decimal_amount), $currency), $currency['decimals'], $currency['decimal_separator'], $currency['thousands_separator']);

  // Build the data array for the cURL request.
  $submit_data = array(
    'SerialNumber' => $payment_method['settings']['skipjack_login_serial_number'],

    // Billing details.
    'SJName' => $billing_address['name_line'],
    'Email' => $order_wrapper->mail->value(),
    'StreetAddress' => $billing_address['thoroughfare'],
    'StreetAddress2' => $billing_address['premise'],
    'City' => $billing_address['locality'],
    'State' => $billing_address['administrative_area'],
    'ZipCode' => $billing_address['postal_code'],
    'Phone' => '0000000000',

    // Shipping details given blank values by default.
    'ShipToName' => '',
    'ShipToStreetAddress' => '',
    'ShipToStreetAddress2' => '',
    'ShipToCity' => '',
    'ShipToState' => '',
    'ShipToZipCode' => '',
    'ShipToPhone' => '0000000000',

    // Order and credit card details.
    'OrderNumber' => $order_wrapper->order_number->value(),
    'AccountNumber' => $cc_info['number'],
    'Month' => $cc_info['exp_month'],
    'Year' => $cc_info['exp_year'],
    'CVV2' => $cc_info['code'],
    'TransactionAmount' => $transaction_amount,
    'OrderString' => $order_string,
  );

  /* If the commerce_shipping module is enabled
   * and this order has a shipping address.
   */
  if (module_exists('commerce_shipping')) {
    if ($order_wrapper->commerce_customer_shipping->value()) {
      $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();

      $shipping_info = array(
        // Shipping details.
        'ShipToName' => $shipping_address['name_line'],
        'ShipToStreetAddress' => $shipping_address['thoroughfare'],
        'ShipToStreetAddress2' => $shipping_address['premise'],
        'ShipToCity' => $shipping_address['locality'],
        'ShipToState' => $shipping_address['administrative_area'],
        'ShipToZipCode' => $shipping_address['postal_code'],
        'ShipToPhone' => '0000000000',
      );

      // Merge the new shipping address information into the submit data array.
      $submit_data = $shipping_info + $submit_data;
    }
  }

  switch ($payment_method['settings']['skipjack_authorize_server']) {
    case 'development':
      $url = 'https://developer.skipjackic.com/scripts/evolvcc.dll?AuthorizeAPI';
      $submit_data['DeveloperSerialNumber'] = $payment_method['settings']['skipjack_developer_serial_number'];
      break;

    case 'production':
      $url = 'https://www.skipjackic.com/scripts/evolvcc.dll?AuthorizeAPI';
      break;
  }

  $post_data = array();
  foreach ($submit_data as $key => $value) {
    if ($value) {
      $post_data[] = $key . '=' . urlencode($value);
    }
  }
  $post_data = implode('&', $post_data);

  // Send the cURL request and retrieve the response.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_VERBOSE, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
  $response = curl_exec($ch);

  if ($error = curl_error($ch)) {
    watchdog('commerce_skipjack', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    drupal_set_message(t('There was an error connecting to the payment gateway.'));
    return FALSE;
  }

  curl_close($ch);

  // Parse the response string into a usable array format.
  $response = explode("\n", $response);
  $keys = str_replace('"', '', preg_split('/","/', $response[0]));
  $values = str_replace('"', '', preg_split('/","/', $response[1]));

  $response = array_combine($keys, $values);

  // Prepare a transaction object to log the API response.
  $transaction = commerce_payment_transaction_new('commerce_skipjack', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->remote_id = $response['szTransactionFileName'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->payload[REQUEST_TIME] = $response;

  if ($response['szIsApproved'] == 1 && $response['szReturnCode'] == 1 && !empty($response['AUTHCODE'])) {
    // Sale approved.
    $message = t('Transaction ID: @id<br />Authorization code: @code', array('@id' => $response['szTransactionFileName'], '@code' => $response['AUTHCODE']));
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->message = $message;
    commerce_payment_transaction_save($transaction);
  }
  else {
    // Sale declined.
    $message = _commerce_skipjack_parse_return_code($response['szReturnCode']);
    drupal_set_message(t('Credit card payment declined: @message', array('@message' => $message)), 'error');
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $transaction->message = $message;
    commerce_payment_transaction_save($transaction);
    return FALSE;
  }
}

/**
 * Helper function to decode the repsonse from Skipjack.
 */
function _commerce_skipjack_parse_return_code($code) {
  switch ($code) {
    case "1": return t("Declined by issuing bank.");
    case "0": return t("Communication Failure");
    case "-1": return t("Error in request - Data was not by received intact by Skipjack Transaction Network.");
    case "-35": return t("Invalid credit card number");
    case "-37": return t("Skipjack is unable to communicate with payment processor.");
    case "-39": return t("Invalid HTML Serial Number.");
    case "-51": return t("The value or length for billing zip code is incorrect.");
    case "-52": return t("The value or length for shipping zip code is incorrect.");
    case "-53": return t("The value or length for credit card expiration month is incorrect.");
    case "-54": return t("Credit card account number incorrect.");
    case "-55": return t("The value or length or billing street address is incorrect.");
    case "-56": return t("The value or length of the shipping address is incorrect.");
    case "-57": return t("The length of the transaction amount must be at least 3 digits.");
    case "-58": return t("Merchant Name associated with Skipjack account is misconfigured or invalid.");
    case "-59": return t("Merchant Address associated with Skipjack account is misconfigured or invalid.");
    case "-60": return t("Length or value in Merchant State misconfigured or invalid.");
    case "-61": return t("The value or length for shipping state/province is empty.");
    case "-62": return t("The value for length orderstring is empty.");
    case "-64": return t("Error invalid phone number.");
    case "-65": return t("The value or length for billing name is empty.");
    case "-66": return t("Error empty e-mail.");
    case "-67": return t("Error empty street address.");
    case "-68": return t("Error empty city");
    case "-69": return t("Error empty state");
    case "-70": return t("Empty zipcode");
    case "-71": return t("Empty ordernumber");
    case "-72": return t("Empty accountnumber");
    case "-73": return t("Empty month");
    case "-74": return t("Empty year");
    case "-75": return t("Empty serialnumber");
    case "-76": return t("Empty transactionamount");
    case "-77": return t("Empty orderstring");
    case "-78": return t("Empty shiptophone");
    case "-79": return t("Length or value sjname");
    case "-80": return t("Length shipto name");
    case "-81": return t("Length or value of Customer location");
    case "-82": return t("The value or length for billing state is empty.");
    case "-83": return t("The value or length for shipping phone is empty.");
    case "-84": return t("There is already an existing pending transaction in the register");
    case "-85": return t("Airline leg field value is invalid or empty.");
    case "-86": return t("Airline ticket info field is invalid or empty");
    case "-87": return t("Point of Sale check routing number is invalid or empty.");
    case "-88": return t("Point of Sale check MICR invalid or empty.");
    case "-89": return t("Point of Sale check MICR invalid or empty.");
    case "-90": return t("Point of Sale check number invalid or empty.");
    case "-91": return t("CVV2 Invalid or empty");
    case "-92": return t("Approval Code Invalid. Approval Code is a 6 digit code.");
    case "-93": return t("'Allow Blind Credits' option must be enabled on the Skipjack Merchant Account.");
    case "-94": return t("Blind Credits Failed");
    case "-95": return t("Voice Authorization Request Refused");
    case "-96": return t("Voice Authorizations Failed");
    case "-97": return t("Fraud Rejection");
    case "-98": return t("Invalid Discount Amount");
    case "-99": return t("POS PIN Debit Pin Block");
    case "-100": return t("Debit-specific Number");
    case "-101": return t("Data for Verified by Visa/MC Secure Code is invalid.");
    case "-102": return t("Authentication Data Not Allowed");
    case "-103": return t("POS Check Invalid Birth Date");
    case "-104": return t("POS Check Invalid Identification Type");
    case "-105": return t("Track Data is in invalid format.");
    case "-106": return t("POS Check Invalid Account Type");
    case "-107": return t("POS PIN Debit Invalid Sequence Number");
    case "-108": return t("Invalid Transaction ID");
    case "-109": return t("Invalid From Account Type");
    case "-110": return t("Pos Error Invalid To Account Type");
    case "-112": return t("Pos Error Invalid Auth Option");
    case "-113": return t("Pos Error Transaction Failed");
    case "-114": return t("Pos Error Invalid Incoming Eci");
    case "-115": return t("POS Check Invalid Check Type");
    case "-116": return t("POS Check Invalid Lane Number");
    case "-117": return t("POS Check Invalid Cashier Number");
    case "-118": return t("Invalid POST URL");
    default: return t("Unknown Skipjack return code");
  }
}
