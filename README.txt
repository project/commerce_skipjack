This integrates the Skipjack API for credit card processing in Drupal Commerce.

Requirements:
- Drupal Commerce
- cURL

To use:
- Enable the module
- Enter your Skipjack HTML and Developer serial numbers on the Skipjack 
Payment Gateway Rule. You can browse to this from 
admin/commerce/config/payment-methods
